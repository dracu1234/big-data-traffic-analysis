# big-data-traffic-analysis

NYPD big data traffic analysis.

## Install

conda install -c esri arcgis ( https://developers.arcgis.com/python/guide/install-and-set-up/ )

## Presentation

For presenting we can use the following extension: https://github.com/damianavila/RISE

Simply install it in conda using: conda install -c damianavila82 rise

A guide + a short video:
https://damianavila.github.io/RISE/index.html
https://www.youtube.com/watch?v=sXyFa_r1nxA&feature=youtu.be