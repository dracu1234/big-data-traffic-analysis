import csv
from datetime import datetime


with open('../data/NYPD_Motor_Vehicle_Collisions.csv',newline='') as csvfile:
    csvreader = csv.reader(csvfile,delimiter=',')
    print("ColumnNames:")
    for row in csvreader:
        for i, column in enumerate(row):
            print(str(i) + " - " + column)
        break
    print("\nDates:")
    dates = []
    killed = 0
    for row in csvreader:
        date = datetime.strptime(row[0],"%m/%d/%Y");
        killed += int(row[11])
        killed = killed + int(row[11])
        dates.append(date);
    print("Start Date:" + min(dates).strftime("%d/%m/%Y"))
    print("End Date:" + max(dates).strftime("%d/%m/%Y"))
    print("Deaths: " + str(killed))
