import json
import csv



def speedLimitsJsonToCsv(inputJson, csvwriter,borough):
    data = json.load(open(inputJson))
    features = data["features"]
    for feature in features:
        prop = feature["properties"];
        print(prop["street"] + ", " + str(prop["postvz_sl"]))
        csvwriter.writerow([borough, prop["street"], prop["postvz_sl"] ])


with open("../data/speed_limit.csv", "w", newline='') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=",")
    csvwriter.writerow([ "borough" ,"street", "postvz_sl"])

    speedLimitsJsonToCsv("../data/speed_limit/speed_limit_bronx.json",csvwriter,"BRONX")
    speedLimitsJsonToCsv("../data/speed_limit/speed_limit_brooklyn.json",csvwriter,"BROOKLYN")
    speedLimitsJsonToCsv("../data/speed_limit/speed_limit_manhattan.json",csvwriter,"MANHATTAN")
    speedLimitsJsonToCsv("../data/speed_limit/speed_limit_queens.json",csvwriter,"QUEENS")
    speedLimitsJsonToCsv("../data/speed_limit/speed_limit_statenisland.json",csvwriter,"STATEN ISLAND")